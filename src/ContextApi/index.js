import React, { Component, createContext } from 'react'
import { Users } from './users'


export const MyConsumer = createContext()
export class MyContextProvider extends Component {
    state = {
        users: Users,
        userDetails: [],
        userModal:false
    }

    userModalFunc=()=>{
        this.setState({
            userModal:!this.state.userModal
        })
    }

    userDetailsFunc = (value) => {
        this.setState({
            userDetails: value
        })
    }
    render() {
        return (
            <MyConsumer.Provider value={{
                ...this.state,
                userDetailsFunc: this.userDetailsFunc,
                userModalFunc:this.userModalFunc
            }}>
                {this.props.children}
            </MyConsumer.Provider>
        )
    }
}

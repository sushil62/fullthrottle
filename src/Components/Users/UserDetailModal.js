import React, { useContext } from 'react'
import { MyConsumer } from '../../ContextApi'
import { Modal} from 'react-bootstrap'
import './userModal.css'

const UserDetailModal = () => {
    const { userDetails, userModalFunc, userModal } = useContext(MyConsumer)

    const { real_name, tz, activity_periods } = userDetails

    const Times = () => {
        if (activity_periods !== undefined) {
            return activity_periods.map((start, index) => {
                return (
                    <div key={index} className="d-flex justify-content-between list">
                        <p>{start.start_time}</p>
                        <p>{start.end_time}</p>
                    </div>
                )
            })
        }
    }

    return (
        <Modal className="modalParent" show={userModal} onHide={() => userModalFunc()}>
            <Modal.Body style={{ borderRadius: "8px" }}>
                <div className="headStyle">
                    <div className="d-flex justify-content-between">
                        <h1 style={{ color: "#fff", fontWeight: "700" }}>{real_name}</h1>
                        <i style={{ color: "#fff", cursor: "pointer" }} onClick={() => userModalFunc()} className="fas fa-times-circle"></i>
                    </div>
                    <h6 style={{ color: "#fff" }}><u>{tz}</u></h6>
                </div>
                <div className="bottomStyle">
                    <h3>Activity Periods</h3>
                    <br />
                    <div className="d-flex justify-content-between p-1">
                        <p style={{ color: "#4caf50" }}><i className="far fa-clock"></i> Start Time</p>
                        <p style={{ color: "#ff0000" }}><i className="far fa-clock"></i> End Time</p>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between">
                        <div style={{ width: "100%" }}>
                            {Times()}
                        </div>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default UserDetailModal

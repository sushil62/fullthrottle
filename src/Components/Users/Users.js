import React, { useContext } from 'react'
import { MyConsumer } from '../../ContextApi'
import UserDetailModal from './UserDetailModal'
import './user.css'


const Users = () => {

    const { users, userDetailsFunc, userDetails,userModalFunc} = useContext(MyConsumer)

    return (
        <div className="usersContainer">
            {users.members.map(user => {
                return (
                    <div onClick={() => {userDetailsFunc(user);userModalFunc()}} className={userDetails.id === user.id?"activeUser":"user"} key={user.id}>
                        <p style={{ fontWeight: "700" }}>{user.real_name}</p>
                        <p className="loc">{user.tz}</p>
                        <div className="line" />
                        <br />
                    </div>
                )
            })}
            <UserDetailModal/>
        </div>
    )
}

export default Users

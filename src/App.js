import React from 'react';
import './App.css';
import Users from './Components/Users/Users'
import fullthrottle from './Images/fullthrottle.png'


function App() {


  return (
    <div className="App">
    <img src={fullthrottle} alt="fullthrottle" style={{height:"50px",marginTop:"10px",marginLeft:"4%"}}/>
      <div className="wrapper">
        <h1 className="Employee">Employee._</h1>
        <br />
        <Users />
      </div>
    </div>
  );
}

export default App;

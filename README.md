This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start or npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `Once page is loaded`

on hovering each user , user name and one border bottom highlight user you hovered it.<br />
on click of any user, Model popup where you can view user details.<br />
if user is selected it highlights with red color as active

